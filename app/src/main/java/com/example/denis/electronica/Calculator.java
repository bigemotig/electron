package com.example.denis.electronica;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by denis on 18.12.14.
 */
public class Calculator extends Fragment{
    private boolean waitOperand = false;
    private boolean notAdded = false;
    private String lastAdded = "";
    private TextView display;
    private Stack <String> stack;
    private Stack <String> operators;
    private boolean clearDisplay = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_calc, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        init(view);
    }

    private void init(View rootView){
        stack       = new Stack<>();
        operators   = new Stack<>();

        display         = (TextView)rootView.findViewById(R.id.display);
        Typeface mDigitalFonts = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/DS-DIGIT.TTF");
        display.setTypeface(mDigitalFonts);

        Button one = (Button) rootView.findViewById(R.id.one);
        Button two     = (Button)rootView.findViewById(R.id.two);
        Button three   = (Button)rootView.findViewById(R.id.three);
        Button four    = (Button)rootView.findViewById(R.id.four);
        Button five    = (Button)rootView.findViewById(R.id.five);
        Button six     = (Button)rootView.findViewById(R.id.six);
        Button seven   = (Button)rootView.findViewById(R.id.seven);
        Button eight   = (Button)rootView.findViewById(R.id.eight);
        Button nine    = (Button)rootView.findViewById(R.id.nine);
        Button zero    = (Button)rootView.findViewById(R.id.zero);
        Button point   = (Button)rootView.findViewById(R.id.point);

        Button plus        = (Button)rootView.findViewById(R.id.plus);
        Button minus       = (Button)rootView.findViewById(R.id.minus);
        Button division    = (Button)rootView.findViewById(R.id.div);
        Button mul         = (Button)rootView.findViewById(R.id.mul);
        Button equally     = (Button)rootView.findViewById(R.id.equal);

        Button del     = (Button)rootView.findViewById(R.id.del);
        Button single  = (Button)rootView.findViewById(R.id.single);

        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDisplayNumber("1");
            }
        });

        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDisplayNumber("2");
            }
        });

        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDisplayNumber("3");
            }
        });

        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDisplayNumber("4");
            }
        });

        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDisplayNumber("5");
            }
        });

        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDisplayNumber("6");
            }
        });

        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDisplayNumber("7");
            }
        });

        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDisplayNumber("8");
            }
        });

        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDisplayNumber("9");
            }
        });

        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDisplayNumber("0");
            }
        });

        point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDisplayNumber(".");
            }
        });

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNewNumber("+");

            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNewNumber("-");
            }
        });

        division.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNewNumber("/");
            }
        });

        mul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNewNumber("*");;
            }

        });

        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isClearDisplay();
            }
        });

        single.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addSingleMinus();
            }
        });

        equally.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                solve();
            }
        });
    }

    /**
     * Решает то, что есть в стеке
     */
    public void solve(){
        if(!operators.isEmpty()) {
            addOperand();       //добавляет
            checkAdded();       //проверяет, было ли добавление, если нет..
            addOperator("=");
        }
    }

    /**
     * Проверяет было ли добавление операнда
     */
    public void checkAdded(){
        if(notAdded){
            if (lastAdded.equals("+") || lastAdded.equals("-")) {
                pushToStack("0.");
            }
            if (lastAdded.equals("*") || lastAdded.equals("/")) {
                waitOperand = false;
                addOperand();
            }
            notAdded = false;
        }
    }

    /**
     * Меняет знак на противоположный
     */
    public void addSingleMinus(){
        if(!display.getText().equals("0.")){
            float aNumber = Float.valueOf(display.getText().toString());
            setLastNumber(String.valueOf(-aNumber));
        }
    }

    public void setNewNumber(String operator){
        addOperand();
        addOperator(operator);
    }

    public void addOperand(){
        if(!waitOperand) {
            String operand = display.getText().toString();
            pushToStack(operand);
            lastAdded = operand;
            clearDisplay = true;
        }
        else {
            notAdded = true;
        }
    }

    public void addOperator(String operator){
        pushOperator(operator);
        if(operators.size() >= 2){
            calcStack();
        }
        if(!operators.lastElement().equals("=")){
            setLastNumber(stack.lastElement());
        }
        else {
            setLastNumber(stack.pop());
            operators.pop();
            waitOperand = false;
        }
    }

    public void calcStack(){
        while (stack.size() != 1){
            int priority2 = getPriority(operators.elementAt(stack.size() - 1));
            int priority1 = getPriority(operators.elementAt(stack.size() - 2));
            if(priority2 > priority1)
                break;
            String operand2 = stack.pop();
            String operand1 = stack.pop();
            String last_operator = operators.get(operators.size() - 2);
            operators.remove(operators.size() - 2);
            pushToStack(calc(operand1, operand2, last_operator));
        }
    }
    public void pushOperator(String operator){
        if(!waitOperand || operator.equals("=")){
            waitOperand = true;
        }
        else {
            operators.pop();
        }
        operators.push(operator);
        lastAdded = operator;
    }
    public void pushToStack(String operand){
        stack.push(operand);
    }
    public void setLastNumber(String lastNumber){
        String pattern      = "\\.0$";
        Pattern regex       = Pattern.compile(pattern);
        Matcher regexMatcher= regex.matcher(lastNumber);

        boolean valid_format = regexMatcher.find();
        if(valid_format){
            lastNumber = lastNumber.replaceAll(pattern,  ".");
        }

        display.setText(lastNumber);
    }

    public void addDisplayNumber(String number){
        waitOperand = false;
        String str = "";
        if(clearDisplay){
            setClearDisplay();
            str = number + ".";
            display.setText(str);
            return;
        }

        str = display.getText().toString();
        if(str.equals("0.")){
            str = number + ".";
        }
        else{
            Matcher regexMatcher;
            Pattern regex;
            boolean valid_format;
            boolean del_point = false;
            regex       = Pattern.compile("\\.$");
            regexMatcher= regex.matcher(str);
            valid_format = regexMatcher.find();
            if(valid_format){
                str = str.replaceAll("\\.$", "");
                str += number;
                del_point = true;
            }
            if(del_point){
                str = str + ".";
            }
        }
        display.setText(str);
    }

    public void setClearDisplay(){
        display.setText("");
        clearDisplay = false;
    }

    public int getPriority(String operator){
        switch (operator){
            case "+":
                return 1;
            case "-":
                return 1;
            case "*":
                return 2;
            case "/":
                return 2;
            case "=":
                return 0;
        }
        return -1;
    }

    public void isClearDisplay(){
        display.setText("0.");
        stack       = new Stack<>();
        operators   = new Stack<>();
    }

    public String calc(String op1, String op2, String operator){
        float operand1 = Float.valueOf(op1);
        float operand2 = Float.valueOf(op2);
        float res = 1;
        switch (operator){
            case "+":
                res = operand1 + operand2;
                break;
            case "-":
                res = operand1 - operand2;
                break;
            case "*":
                res = operand1 * operand2;
                break;
            case "/":
                res = operand1 / operand2;
                break;
        }
        return String.valueOf(res);
    }
}
